using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common;
public partial class SaveDataManager : Singleton <SaveDataManager>
{
    private PlayerCfg m_playerCfg;
    public PlayerCfg PlayerCfg
    {
        get
        {
            //event;

            return m_playerCfg;
        }
        set
        {
            //event

            m_playerCfg = value;
        }
    }
    void InitData()
    {
        m_playerCfg = Resources.Load<PlayerCfg>("Data/PlayerConfig");
    }
    protected override void Awake()
    {
        base.Awake();
        InitData();
    }

}
