using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class SceneStateBase
{
    private string _StateName = "Scene";
    public string StateName
    {
        get{return _StateName;}
        set{_StateName = value;}
    }
    protected SceneStateManager _SceneStateManager = null;
    public SceneStateBase(SceneStateManager m)
    {
        _SceneStateManager = m;
    }
    public virtual void StateBegin()
    {
        Debug.Log($"{StateName} => Begin");
    }
    public virtual void StateUpdate()
    {
        Debug.Log($"{StateName} => End");
    }
    public virtual void StateEnd()
    {
        Debug.Log($"{StateName} => End");
    }
}
