using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Common;
public class SceneStateManager: Singleton<SceneStateManager>
{
    private SceneStateBase _sceneBase;
    bool isBegin = false;
    public SceneStateManager(){}
    /// <summary>
    /// 设置场景状态
    /// </summary>
    /// <param name="sceneState"></param>
    /// <param name="loadSceneName"></param>
    public void SetSceneState(SceneStateBase sceneBase ,string loadSceneName)
    {
        isBegin = false;
        LoadScene(loadSceneName);
        if(_sceneBase != null)//清空
        {
            _sceneBase.StateEnd();
        }
        _sceneBase = sceneBase;

    }
    /// <summary>
    /// 加载场景
    /// </summary>
    /// <param name="loadSceneName"></param>
    private void LoadScene(string loadSceneName)
    {
        if(loadSceneName == null || loadSceneName.Length == 0)
        {
            return;
        }
        SceneManager.LoadScene(loadSceneName);
    }
    /// <summary>
    /// 场景Update
    /// </summary>
    public void StateUpdate()
    {
        if(_sceneBase != null && isBegin == false)
        {
            _sceneBase.StateBegin();
            isBegin = true;
        }
        if(_sceneBase != null)
        {
            _sceneBase.StateUpdate();
        }
    }

}
