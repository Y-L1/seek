using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartScene : SceneStateBase
{
    public StartScene(SceneStateManager m) : base(m)
    {
        this.StateName = "StartScene";//场景状态是开始场景
    }
    public override void StateBegin()
    {
        base.StateBegin();
        //加载资源，初始化，热更新
    }
    public override void StateUpdate()
    {
        base.StateUpdate();
        SceneStateManager.Instance.SetSceneState(new SceneStateBase(SceneStateManager.Instance),"Scene1");
    }
    public override void StateEnd()
    {
        base.StateEnd();
    }
}
