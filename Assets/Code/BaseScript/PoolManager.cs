using System.Security.Cryptography.X509Certificates;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common;
public class PoolManager : Singleton<PoolManager>
{
    public Dictionary<string,List<GameObject>> poolDic = new Dictionary<string, List<GameObject>>();
    /// <summary>
    /// 往池外拿东西
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public GameObject GetObj(string name)
    {
        GameObject obj = null;
        if(poolDic.ContainsKey(name) && poolDic[name].Count > 0)
        {
            obj = poolDic[name][0];//用二维数组存放
            poolDic[name].RemoveAt(0);
        }
        else
        {
            obj = GameObject.Instantiate(Resources.Load<GameObject>(name));
            obj.name = name;
        }
        obj.SetActive(true);
        return obj;
    }
    /// <summary>
    /// 放东西进池里
    /// </summary>
    /// <param name="name"></param>
    /// <param name="obj"></param>
    public void PushObj(string name,GameObject obj)
    {
        obj.SetActive(false);
        if(poolDic.ContainsKey(name))
        {

        }
        else
        {
            poolDic.Add(name,new List<GameObject>() { obj });
        }

    }
}
