using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace Logic
{
    public class DataUtility
    {
        public static T LoadFromRemoteOrLocalOrDefault<T>(string json, T temple) where T : ScriptableObject
        {
            try
            {
                if (!string.IsNullOrEmpty(json))
                {
                    return LoadFromJsonString<T>(json);
                }
                else
                {
                    return LoadFromJSONorDefault(temple);
                }
            }
            catch (System.Exception ex)
            {
                Debug.LogWarning("LoadFromRemoteOrLocalOrDefault fail use default " + ex);
                return InitializeFromDefault(temple);
            }
        }

        public static T LoadFromJSONorDefault<T>(T temple) where T : ScriptableObject
        {
            string filePath = SavedDataPath(typeof(T).ToString());
            if (System.IO.File.Exists(filePath))
            {
                return LoadFromJSON<T>(filePath);
            }
            else
            {
                Debug.Log("LoadDefault , file not exist" + filePath);
                return InitializeFromDefault<T>(temple);
            }
        }

        public static T InitializeFromDefault<T>(T settings) where T : ScriptableObject

        {
            T instance = UnityEngine.Object.Instantiate(settings);
            instance.hideFlags = HideFlags.HideAndDontSave;
            return instance;
        }

        public static T Create<T>() where T : ScriptableObject
        {
            T instance = ScriptableObject.CreateInstance<T>();
            instance.hideFlags = HideFlags.HideAndDontSave;
            return instance;
        }

        public static T LoadFromSavedJSON<T>() where T : ScriptableObject
        {
            string filePath = SavedDataPath(typeof(T).ToString());
            if (System.IO.File.Exists(filePath))
            {
                return LoadFromJSON<T>(filePath);
            }
            else
            {
                return Create<T>();
            }
        }

        public static T LoadFromJSON<T>(string path) where T : ScriptableObject
        {
            string textData = System.IO.File.ReadAllText(path);
// #if UNITY_EDITOR || UNITY_STANDALONE
            string xorData = textData;
// #else
//             string xorData = EncryptorDecryptor.EncryptDecrypt(textData);
// #endif
            return LoadFromJsonString<T>(xorData);
        }

        public static T LoadFromJsonString<T>(string json) where T : ScriptableObject
        {
            T instance = ScriptableObject.CreateInstance<T>();
            JsonUtility.FromJsonOverwrite(json, instance);
            instance.hideFlags = HideFlags.HideAndDontSave;
            return instance;
        }

        public static void SaveToJSON<T>(T instance) where T : ScriptableObject
        {
            string path = SavedDataPath(typeof(T).ToString());
            Debug.LogFormat("Saving DataStore to {0}", path);
            string data = JsonUtility.ToJson(instance, true);
            Debug.Log(data);
            SaveToJsonFile(path, data);
        }

        public static void SaveToJsonFile(string path, string data)
        {
// #if UNITY_EDITOR || UNITY_STANDALONE
            string xorData = data;
// #else
//             string xorData = EncryptorDecryptor.EncryptDecrypt(data);
// #endif
            System.IO.FileInfo file = new System.IO.FileInfo(path);
            file.Directory.Create();
            System.IO.File.WriteAllText(path, xorData);
        }

        public static string SavedDataPath(string name)
        {
#if UNITY_EDITOR
            string jsonPath = Path.Combine(Application.dataPath, @"../Library/SavedData/" + name + ".json");
            string fullPath = Path.GetFullPath(jsonPath);
            return fullPath;
#else
            return Path.Combine(Application.persistentDataPath, name + ".json");
#endif
        }

        public static string SavedDataDirPath(string folderName)
        {
#if UNITY_EDITOR
            string dirPath = Path.Combine(Application.dataPath, @"../Library/SavedData/" + folderName);
            string fullPath = Path.GetFullPath(dirPath);
            return fullPath;
#else
            return Path.Combine(Application.persistentDataPath, folderName);
#endif
        }

        #region JSON Setting
        public static string JsonDataPath(string name)
        {
            string jsonPath = Path.Combine(Application.dataPath, @"../Library/Json/", name + ".json");
            string fullPath = Path.GetFullPath(jsonPath);
            return fullPath;
        }
        #endregion



    }
}
