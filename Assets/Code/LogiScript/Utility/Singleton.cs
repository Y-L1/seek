using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Common
{
    /// <summary>
    /// 单例模式
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Singleton<T> :MonoBehaviour where T :Singleton<T>
    {
        private static T instance; //创建单例

        /// <summary>
        /// 获取单例实例
        /// </summary>
        /// <value></value>
        public static T Instance
        {
            get {return instance;}

        }
        protected virtual void Awake() //在Awake中赋值，允许子类继承和修改
        {
            if(instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                instance = (T)this;
            }
        }
        public static bool IsInitialized //判断单例是否已经生成
        {
            get{return instance != null;}
        }
        protected virtual void OnDestory()//在销毁时设置为空
        {
            if(instance == this) instance = null;
        }

    }

}

