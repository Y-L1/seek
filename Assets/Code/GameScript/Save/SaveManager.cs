using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;//序列化，Binary转为二进制
public class SaveManager : MonoBehaviour
{
    public Inveroty myInventory;//保存我的背包

    public void Do_Save()
    {
        Debug.Log(Application.persistentDataPath);//Application.persistentDataPath在windous,安卓，ios下的文件夹路径不同，可以输出查看
        if (!Directory.Exists(Application.persistentDataPath + "/Save_Datas"))
        {
            Directory.CreateDirectory(Application.persistentDataPath + "/Save_Datas");//如果该路径下不存在Save_Datas文件夹，就创建Save_Datas文件夹
        }

        BinaryFormatter formatter = new BinaryFormatter();//二进制转化
        FileStream file = File.Create(Application.persistentDataPath + "Save_Datas/inventory.txt");

        var json = JsonUtility.ToJson(myInventory);//变成json格式

        formatter.Serialize(file, json);//逻辑：将myInventory转换成json格式，用二进制的方法写进file文件中

        file.Close();
        
    }

    public void Do_Load()
    {
        Debug.Log(Application.persistentDataPath);

        BinaryFormatter bf = new BinaryFormatter();
        if (File.Exists(Application.persistentDataPath + "Save_Datas/inventory.txt"))
        {
            FileStream file = File.Open(Application.persistentDataPath + "Save_Datas/inventory.txt",FileMode.Open);

            JsonUtility.FromJsonOverwrite((string)bf.Deserialize(file),myInventory);

            file.Close(); 
        }
       
    }


}
