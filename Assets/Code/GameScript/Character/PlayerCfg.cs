using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerStateType
{
    Idel = 0,
    Walk, 
    Run,
    JumpStart,
    JumpLoop,
    Die,
}

[CreateAssetMenu(fileName = "new playerCfg",menuName = "Cfg/playerCfg" )]
public class PlayerCfg : ScriptableObject
{
    public int m_hp;//Ѫ��
    public int m_mp;//����
    public PlayerStateType m_playerState;//��ɫ״̬

}
