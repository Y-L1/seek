using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Common;

public interface Character
{
    public abstract void Move();
    public abstract void Attack();
    public abstract void Hurt();
    public abstract void Death();
    public abstract void Buff();//增益
    public abstract void DeBuff();//负增益
    

}
public class Player : Singleton<Player>,Character
{
    [Header("玩家")]
    private Rigidbody2D Rb;
    private bool m_isGround;
    [Header("移动")]
    public float m_speed = 100f;
    [Header("跳跃")]
    public Transform GroundCheck;
    public LayerMask Ground;
    private bool m_jumpPressed;
    private bool m_isJump;
    private int m_jumpCount = 2;    //最大连跳次数 当前为2连跳
    public float m_jumpForce = 10f;

    [Header("动画")]
    private Animator Anim;

    public void Attack()
    {
        
    }

    public void Buff()
    {
        
    }

    public void Death()
    {
        
    }

    public void DeBuff()
    {
        
    }

    public void Hurt()
    {
        //SaveDataManager.Instance.PlayerCfg.m_hp -= 1;
    }

    public void Move()
    {
        float horizontalmove = Input.GetAxis("Horizontal");/*获得-1~1*/
        float facedirection = Input.GetAxisRaw("Horizontal");/*获得-1，0，1*/


        /*角色移动*/
        Rb.velocity = new Vector2(horizontalmove * m_speed * Time.deltaTime, Rb.velocity.y);


        /*角色朝向*/
        if (facedirection != 0)
        {
            transform.localScale = new Vector3(facedirection, 1, 1);
        }
    }
    public void Jump()
    {
        if (m_isGround)
        {
            m_jumpCount = 2;
            m_isJump = false;
        }
        if (m_jumpPressed && m_isGround)
        {
            m_isJump = true;
            Rb.velocity = new Vector2(Rb.velocity.x, m_jumpForce * Time.deltaTime);
            m_jumpCount--;
            m_jumpPressed = false;

        }
        else if (m_jumpPressed && m_jumpCount > 0 && !m_isGround)
        {
            Rb.velocity = new Vector2(Rb.velocity.x, m_jumpForce * Time.deltaTime);
            m_jumpCount--;
            m_jumpPressed = false;
        }
    }

    private void Init()
    {
        Rb = transform.GetComponent<Rigidbody2D>();
        Anim = transform.GetComponent<Animator>();
    }
    protected override void Awake()
    {
        base.Awake();
        Init();
    }
    void Start()
    {

    }

    private void FixedUpdate()
    {
        //地面检测
        m_isGround = Physics2D.OverlapCircle(GroundCheck.position, 0.1f, Ground);
        Move();
        Jump();
        SwitchAnim();
    }

    void Update()
    {
        //jump检测
        if (Input.GetButtonDown("Jump") && m_jumpCount > 0)
        {
            m_jumpPressed = true;
        }
    }
    void SwitchAnim()
    {
        //左右移动动画
        Anim.SetFloat("speed", Mathf.Abs(Input.GetAxis("Horizontal")));

        if (m_isGround)
        {
            Anim.SetBool("isGround", true);
        }
        else
        {
            Anim.SetBool("isGround", false);
        }

        //跳跃
        if (m_isGround)
        {
            Anim.SetBool("isFalling", false);
            Anim.SetBool("isJumping", false);
        }
        else if (Rb.velocity.y > 0)
        {
            Anim.SetBool("isJumping", true);
        }
        else if (Rb.velocity.y < 0)
        {
            Anim.SetBool("isJumping", false);
            Anim.SetBool("isFalling", true);
        }

    }
}
