using Common;
using System.Collections.Generic;
using UnityEngine;

namespace UIFramework
{
    public class UIManager : Singleton<UIManager>
    {
        public GameObject[] TransRootArray;
        private List<UIBase> UIRuntimeList = new List<UIBase>();
        private Stack<UIBase> UIBackStack = new Stack<UIBase>();

        [SerializeField]
        private List<GameObject> UIPrefabList;
        public enum UILayer
        {
            Bottom = 0,
            NormalBack = 1,
            Normal = 2,
            NormalFront = 3,
            Top = 4,
        }
        private void Start()
        {
            DontDestroyOnLoad(this);
            UIRuntimeList = new List<UIBase>();
            this.gameObject.transform.position = new Vector3(0,1,0);
        }
        #region api
        /// <summary>
        /// 获取Wnd的父物体
        /// </summary>
        /// <param name="_UILayer"></param>
        /// <returns></returns>
        public Transform GetTransRoot(UILayer _UILayer)
        {
            return TransRootArray[(int)_UILayer].transform;
        }
        /// <summary>
        /// 从RuntimeList中移除Wnd
        /// </summary>
        /// <param name="ub"></param>
        public void RemoveFromRuntimeList(UIBase Ub)
        {
            UIRuntimeList.Remove(Ub);
        }
        /// <summary>
        /// 关闭Wnd
        /// </summary>
        /// <param name="ub"></param>
        public void CloseWnd(UIBase ub)
        {
            ub.gameObject.SetActive(false);
            if(ub._Backable)
            {
                UIBackStack.Push(ub);
            }
            ub.OnUIClose();
        }
        /// <summary>
        /// 查找Wnd
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T FindUIWnd<T>()
        {
            foreach (var wndRuntimeObj in UIRuntimeList)
            {
                if (wndRuntimeObj.TryGetComponent<T>(out T component))
                {
                    return component;
                }
            }
            return default(T);
        }
        /// <summary>
        /// 创建或打开Wnd
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T CreateOrOpenUIWnd<T>()
        {
            foreach (var wndRuntimeObj in UIRuntimeList)
            {
                if (wndRuntimeObj.TryGetComponent<T>(out T component))
                {
                    wndRuntimeObj.OnUIOpen();
                    return component;
                }
            }
            foreach (var wndPrefab in UIPrefabList)
            {
                if (wndPrefab.TryGetComponent<T>(out T _component))
                {
                    GameObject wndObj = Instantiate(wndPrefab);
                    UIBase ub = wndObj.GetComponent<UIBase>();
                    UIRuntimeList.Add(ub);
                    ub.OnUIOpen();
                    return wndObj.GetComponent<T>();
                }
            }
            Debug.LogError("[UIManager] Can't Find or Open UIWnd");
            return default(T);
        }
        /// <summary>
        /// 跳转Wnd
        /// </summary>
        /// <param name="currentWnd"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T JumpToWnd<T>(UIBase currentWnd)
        {
            CloseWnd(currentWnd);
            return CreateOrOpenUIWnd<T>();
        }
        /// <summary>
        /// 页面回退
        /// </summary>
        /// <param name="currentWnd"></param>
        /// <returns></returns>
        public UIBase Fallback(UIBase currentWnd)
        {
            CloseWnd(currentWnd);
            if(UIBackStack != null && UIBackStack.Count > 0)
            {
                UIBase fallbackWnd = UIBackStack.Pop();
                fallbackWnd.OnUIOpen();
                return fallbackWnd;
            }
            return null;
        }
        #endregion
    }
}
