using UnityEngine;
using static UIFramework.UIManager;

namespace UIFramework
{
    public class UIBase : MonoBehaviour
    {
        public UILayer _UILayer;
        [Header("是否可回退至此页面")]
        public bool _Backable;
        protected string _UIName;
        protected UIManager _UIManager;
        protected RectTransform _RectTrans;

        public virtual void OnUIOpen()
        {
            _UIName = gameObject.name;
            Debug.Log($"{_UIName} => OnUIOpen");
            _RectTrans = GetComponent<RectTransform>();
            _UIManager = UIManager.Instance;
            transform.SetParent(_UIManager.GetTransRoot(_UILayer));
            transform.localScale = Vector3.one;//Vectors(1,1,1)
            _RectTrans.offsetMin = Vector2.zero;
            _RectTrans.offsetMax = Vector2.zero;
            gameObject.SetActive(true);
        }
        public virtual void OnUIClose()
        {
            Debug.Log($"{_UIName} => OnUIClose");
        }
        public virtual void Refresh()
        {
            Debug.Log($"{_UIName} => Refresh");
        }
    }
}
